#include<iostream>
#include<fstream>
#include<sstream>
#include<stdio.h>
#include<stdlib.h>
#include<vector>
#include<string.h>
#include<tuple>
#include<map>
#include<unordered_map>
#include<boost/format.hpp>

using namespace std;
using boost::format;


typedef long long Cost;


vector<size_t> residue_index_map;

struct wcsptuple {
  vector<size_t> tup;
  Cost cost;
};

struct wcspfunc {
  Cost defcost;
  vector<size_t> scope;
  vector< wcsptuple > specs;

  size_t arity() const { return scope.size(); }
};

struct wcsp {
  string name;
  Cost ub;	
  vector<size_t> domains;
  size_t nvars() const { return domains.size(); }	
  vector<wcspfunc> functions;
};

char three2one(string res)
{
  if(!res.compare("ALA")) return 'A';
  else if(!res.compare("ARG")) return 'R';
  else if(!res.compare("ASN")) return 'N';
  else if(!res.compare("ASP")) return 'D';
  else if(!res.compare("CYS")) return 'C';
  else if(!res.compare("GLU")) return 'E';
  else if(!res.compare("GLN")) return 'Q';
  else if(!res.compare("GLY")) return 'G';
  else if(!res.compare("HIS")) return 'H';
  else if(!res.compare("ILE")) return 'I';
  else if(!res.compare("LEU")) return 'L';
  else if(!res.compare("LYS")) return 'K';
  else if(!res.compare("MET")) return 'M';
  else if(!res.compare("PHE")) return 'F';
  else if(!res.compare("PRO")) return 'P';
  else if(!res.compare("SER")) return 'S';
  else if(!res.compare("THR")) return 'T';
  else if(!res.compare("TRP")) return 'W';
  else if(!res.compare("TYR")) return 'Y';
  else if(!res.compare("VAL")) return 'V';
  else
    {
      cout << "Unrecognized residue: " << res << endl; 
      exit(1);
    }
}


tuple<size_t, size_t, string> get_unary_constraint(string interaction)
{
  string delim="-";
  string token;
  string residue_t;
  size_t pos=0;
  size_t x;
  size_t x_val;
  pos=interaction.find(delim);
  token=interaction.substr(0,pos);
  x=atoi(token.c_str());
  bool absent=true;
  for(size_t i=0;i<residue_index_map.size();i++)
    if (residue_index_map[i]==x)
      {
        x= i+1;
        absent=false;
        break;
      }
  if (absent)
    {
      residue_index_map.push_back(x);
      x= residue_index_map.size();
    }
  interaction.erase(0,token.length()+1);
  pos=interaction.find(delim);
  token=interaction.substr(0,pos);
  residue_t = token;
  interaction.erase(0,token.length()+1);
  pos=interaction.find(delim);
  token=interaction.substr(0,pos);
  x_val=atoi(token.c_str());
  return make_tuple(x,x_val,residue_t);
}

tuple<size_t,size_t,string,size_t,size_t,string> get_binary_constraint(string interaction)
{
  string delim="::";
  string token;
  size_t pos=0;
  size_t x,y;
  size_t x_val,y_val;
  string x_type,y_type;
  pos=interaction.find(delim);
  token=interaction.substr(0,pos);
  tie(x,x_val,x_type)=get_unary_constraint(token);
  interaction.erase(0,token.length()+2);
  tie(y,y_val,y_type)=get_unary_constraint(interaction);
  return make_tuple(x,x_val,x_type,y,y_val,y_type);
}

void shift_and_fill_wcsp(wcsp & _wcsp, Cost min)
{
  Cost ub=0;
  Cost f_max;
  for(auto& f:_wcsp.functions)
    {
      f_max=0;
      for(auto& s:f.specs)
        {
          s.cost-=min;
          if (s.cost>f_max)   
            f_max=s.cost;
        }
      ub+=f_max;
    }
  ub+=1;
  for(auto& f:_wcsp.functions)
    f.defcost=ub;
  _wcsp.ub=ub;
}

void fill_wcsp(wcsp & _wcsp)
{
  Cost ub=0;
  Cost f_max;
  for(auto& f:_wcsp.functions)
    {
      f_max=0;
      for(auto& s:f.specs)
        {
          if (s.cost>f_max)
            f_max=s.cost;
        }
      ub+=f_max;
    }
  ub+=1;
  for(auto& f:_wcsp.functions)
    f.defcost=ub;
  _wcsp.ub=ub;
}

tuple< wcsp, vector< vector<string> > > load_matrix(const char* _osprey_file)
{
  ifstream ospdata(_osprey_file);
  if (not ospdata.is_open())
    {
      cout << "Couldn't open matrix file " << endl;
      exit(1);
    }
  wcsp _wcsp;
  _wcsp.name=basename(_osprey_file);
  wcspfunc _wcspfunc;
  vector< string > _domaindesc;
  vector< vector<string> > _domainsdescs;
  istringstream line;
  string s;
  string type;
  string interaction;
  long double dcost;
  Cost cost;
  Cost min=0;
  size_t x=0;
  size_t y=0;
  size_t x_val=0;
  size_t y_val=0;
  string x_type;
  string y_type;
  size_t prev_x=0;
  size_t prev_y=0;
  size_t domain_count=0;
  bool uptodate=true;
  cout.precision(16);
  while(!ospdata.eof())
    {
      getline(ospdata,s);
      line.str(s);
      line.clear();
      if (line.str().empty())
        {
          line.str().clear();
        }
      else
        {
          line >> type >> interaction >> dcost;
          if (type=="Etmp")
            {}
          else if (type=="ROT::BKB") 
            {
              tie(x,x_val,x_type)=get_unary_constraint(interaction);
              cost = (Cost) round(pow(10,8)*dcost);

              if(prev_x!=x)
                {
                  if (!uptodate)
                    {
                      _wcsp.functions.push_back(_wcspfunc);
                      _wcsp.domains.push_back(domain_count);
                      _domainsdescs.push_back(_domaindesc);
                      uptodate=true;
                    }
                  else
                    min=cost;
                  _wcspfunc.specs.clear();
                  _wcspfunc.scope.clear();
                  _wcspfunc.defcost=0;
                  _wcspfunc.scope.push_back(x-1);
                  _domaindesc.clear();
                  domain_count=0;
                }
              wcsptuple _tuple; 
              _tuple.tup.push_back(x_val-1);
              uptodate=false;
              _tuple.cost=cost;
              _wcspfunc.specs.push_back(_tuple);
              _domaindesc.push_back(x_type);
              prev_x=x;
              domain_count++;              
              if (cost<min)
                min=cost;
            }
          else if (type=="ROT::ROT")
            {
              tie(x,x_val,x_type,y,y_val,y_type)=get_binary_constraint(interaction);
              cost = (Cost) round(pow(10,8)*dcost);

              if (prev_x!=x || prev_y!=y)
                {
                  if (!uptodate)
                    {
                      _wcsp.functions.push_back(_wcspfunc);
                      uptodate=true;
                    }
                  else
                    min=cost;
                  _wcspfunc.specs.clear();
                  _wcspfunc.scope.clear();
                  _wcspfunc.defcost=0;
                  _wcspfunc.scope.push_back(x-1);
                  _wcspfunc.scope.push_back(y-1);
                }
              wcsptuple _tuple; 
              _tuple.tup.push_back(x_val-1);
              uptodate=false;
              _tuple.tup.push_back(y_val-1);
              _tuple.cost=cost;
              _wcspfunc.specs.push_back(_tuple);
              prev_x=x;
              prev_y=y;
              if (cost<min)
                min=cost;
            }
          else if (type=="ROT::SELF")
            {}
          else
            {
              cerr << "Error: unknown interaction type: " << type << endl;
              exit(1);
            }
        }
    }
  if (!uptodate)
    {
      _wcsp.functions.push_back(_wcspfunc);
	if (_domaindesc.size())
	  {	
      	_wcsp.domains.push_back(domain_count);
      	_domainsdescs.push_back(_domaindesc);
	  }
    }
  shift_and_fill_wcsp(_wcsp,min);
  return make_tuple(_wcsp,_domainsdescs);
}


void write_wcsp(wcsp const& w, vector< vector<string> >& dd,ostream &ofs)
{
  size_t maxd = *max_element(w.domains.begin(), w.domains.end());
  ofs << w.name << ' ' << w.nvars()
      << ' ' << maxd
      << ' ' << w.functions.size() << ' ' << w.ub << "\n";

  for(auto& d : w.domains)
    ofs << d << ' ';
  ofs << "\n";

  for(auto& f: w.functions) {
    ofs << f.arity() << ' ';
    for(auto& v : f.scope)
      ofs << v << ' ';
    ofs << f.defcost << ' ' << f.specs.size() << "\n";
    for(auto& s : f.specs) {
      for(auto& v : s.tup)
        ofs << v << ' ';
      ofs << min(s.cost, w.ub) << "\n";
    }
  }
  for(size_t d=0; d<dd.size(); d++)
    {
      for(auto& a: dd[d])
        ofs << three2one(a) << " ";
      ofs << endl;
    }
}


void write_wcsp(wcsp & w, vector< vector<string> >& dd,ostream &ofs, int step)
{
  for(size_t i=0;i<w.domains.size()-step;i++)
    {
      wcspfunc _fun;
      _fun.scope.push_back(i);
      _fun.scope.push_back(i+step);
      _fun.defcost = w.ub;
      for(size_t j=0;j<w.domains[i];j++)
        for(size_t k=0;k<w.domains[i+step];k++)
          {
            wcsptuple tupl;
            tupl.tup.push_back(j);
            tupl.tup.push_back(k);
            
            if(dd[i][j] == dd[i+step][k])
              tupl.cost = 0;
            else
              tupl.cost = w.ub*(w.domains.size()-step);
            _fun.specs.push_back(tupl);
          }
      if (_fun.specs.size())
        w.functions.push_back(_fun);
      else
        {
          cerr << "Couldn't find any symmetry between residues " << i << " and " << i+step << endl;
          exit(1);
        }
    }
  size_t maxd = *max_element(w.domains.begin(), w.domains.end());
  ofs << w.name << ' ' << w.nvars()
      << ' ' << maxd
      << ' ' << w.functions.size() << ' ' << w.ub << "\n";
  
    
  for(auto& d : w.domains)
    ofs << d << ' ';
  ofs << "\n";

  for(auto& f: w.functions) {
    ofs << f.arity() << ' ';
    for(auto& v : f.scope)
      ofs << v << ' ';
    ofs << f.defcost << ' ' << f.specs.size() << "\n";
    for(auto& s : f.specs) {
      for(auto& v : s.tup)
        ofs << v << ' ';
      ofs << min(s.cost, w.ub) << "\n";
    }
  }
  for(size_t d=0; d<dd.size(); d++)
    {
      for(auto& a: dd[d])
        ofs << a << " ";
      ofs << endl;
    }
}

int main(int argc, char* argv[])
{
  wcsp _wcsp;
  vector< vector<string> > _domainsdescs;
  tie(_wcsp,_domainsdescs)=load_matrix(argv[1]);
  ofstream f(argv[2]);
  if(f.is_open())
    {
      if (argc==4)
        {
          int step;
          cout << "Handling symmetry..." << endl;
          step = atoi(argv[3]);
          write_wcsp(_wcsp, _domainsdescs, f, step);
        }
      else
        write_wcsp(_wcsp, _domainsdescs, f);
      f.close();
    }
  else
    {
      cerr << "Couldn't open output file " << argv[2] << endl;
      exit(1);
    }
        
  return EXIT_SUCCESS;
}
