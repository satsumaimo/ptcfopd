CC=g++
CFLAGS=-W -Wall -std=c++11
EXEC=mat2wcsp

all:	
	$(CC) $(EXEC).cc -o $(EXEC) $(CFLAGS)

clean:
	rm -f *.o $(EXEC)
