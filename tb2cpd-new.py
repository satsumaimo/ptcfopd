import optparse
import numpy
import math
import os
import re
import rosetta
from rosetta.core.pack.rotamer_set import *
from rosetta.core.pack.interaction_graph import *
from rosetta.core.pack import *
from rosetta.core.scoring import *
from rosetta.core.graph import *
from toolbox import *


def relax_backbone(pose,nbrelax):
    score_fxn = create_score_function('talaris2014')
    refinement = FastRelax(score_fxn)
    jd = PyJobDistributor(pose.pdb_info().name()[:-4], nbrelax, score_fxn)
    jd.native_pose = pose
    decoy=Pose()
    best=Pose()
    best.assign(pose)
    while not jd.job_complete:
        decoy.assign(pose)
        resn=1
        refinement.apply(decoy)
        jd.output_decoy(decoy)
        if (score_fxn(decoy)<score_fxn(best)):
            best.assign(decoy)
    return best

def compute_interactions(pose,resfile,pdb_out):
    score_fxn = create_score_function('talaris2014')
    task_design = TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    parse_resfile(pose, task_design, resfile)
    pose.update_residue_neighbors()
    png = create_packer_graph(pose, score_fxn, task_design) #Uncomment for latest Pyrosetta versions
    rotsets = RotamerSets()  
    ig = pack_rotamers_setup(pose, score_fxn, task_design, rotsets)
    ig = InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(task_design, rotsets, pose, score_fxn, png) #Uncomment for latest Pyrosetta versions 
#    setup_IG_res_res_weights(pose, task_design, rotsets, ig) #Comment for latest Pyrosetta versions
    f = open(pdb_out,'w')
    ener=0.0
    f.write("Etmp = 0\n")
    for res1 in range(1,ig.get_num_nodes()+1):
        rotn=0
        prevresname=""
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            ener=str(ig.get_one_body_energy_for_node_state(res1,i))
            pres1 = rotsets.moltenres_2_resid(res1)
            resname = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name3()
            f.write("ROT::BKB\t"+str(res1)+"-"+resname+"-"+str(i)+" "+ener+" "+ener+'\n')
            prevresname=resname
    for res1 in range(1,ig.get_num_nodes()+1):
        for res2 in range(res1+1,ig.get_num_nodes()+1):
            if (ig.get_edge_exists(res1, res2)):
                rotn1=0
                prevres1name=""
                pres1 = rotsets.moltenres_2_resid(res1)
                for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
                    nres1=rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name3()
                    prevres1name=nres1
                    rotn2=0
                    prevres2name=""
                    pres2 = rotsets.moltenres_2_resid(res2)
                    for j in range(1, rotsets.rotamer_set_for_moltenresidue(res2).num_rotamers()+1):
                        ener=str(ig.get_two_body_energy_for_edge(res1,res2,i,j))
                        nres2=rotsets.rotamer_set_for_moltenresidue(res2).rotamer(j).name3()
                        f.write("ROT::ROT\t"+str(res1)+"-"+nres1+"-"+str(i)+"::"+str(res2)+"-"+nres2+"-"+str(j)+" "+ener+" "+ener+'\n')
                        prevres2name=nres2
    for res1 in range(1,ig.get_num_nodes()+1):
        rotn=0
        prevresname=""
        pres1 = rotsets.moltenres_2_resid(res1)
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            ener= str(0.0) 
            resname = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name3()
            if (resname!=prevresname):
                rotn = 1
            else:
                rotn+=1
            f.write("ROT::SELF\t"+str(res1)+"-"+resname+"-"+str(rotn)+" "+ener+" "+ener+'\n')
            prevresname=resname

    f.close()
    return pose
                                 

def provide_ub(pose, pdb_file, resfile, outf):
    score_fxn = create_score_function('talaris2014')
    trial_pose = []
    for i in range(0,9):
        trial_pose.append(Pose())
        trial_pose[i] = pose_from_pdb(pdb_file)
    task_design = TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    parse_resfile(pose, task_design, resfile)
    mover = PackRotamersMover(score_fxn, task_design)
    mover.apply(trial_pose[0])
    min = score_fxn(trial_pose[0])
    ig = mover.ig()
    for i in range(1,9):
        mover.apply(trial_pose[i])
        if (score_fxn(trial_pose[i])<min):
            ig = mover.ig()
            min = score_fxn(trial_pose[i])
    sol=""
    for res in range(1,ig.get_num_nodes()+1):
        sol+=str(ig.get_node(res).get_current_state()-1)+" "        
    f = open(outf,'w')
    f.write(sol+'\n')
    f.close()

def load_assign(pose, assign, out_file, resfile):
    copy_pose = Pose()
    copy_pose.assign(pose)
    score_fxn = create_score_function('talaris2014')
    task_design = TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    parse_resfile(pose, task_design, resfile)
    pose.update_residue_neighbors()
    png = create_packer_graph(pose, score_fxn, task_design)  #Uncomment for latest Pyrosetta versions
    rotsets = RotamerSets()
    ig = pack_rotamers_setup(pose, score_fxn, task_design, rotsets)
    ig = InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(task_design, rotsets, pose, score_fxn, png)  #Uncomment for latest Pyrosetta versions
#    setup_IG_res_res_weights(pose, task_design, rotsets, ig) #Comment for latest Pyrosetta versions
    mat = numpy.loadtxt(assign, dtype=int)
    for i in range(0, len(mat)):
        res = rotsets.rotamer_set_for_moltenresidue(i+1).rotamer(int(mat[i]+1))
        copy_pose.replace_residue(rotsets.moltenres_2_resid(i+1), res, False)
    copy_pose.dump_pdb(out_file)

def dump_pairwise_energies(pose, assign, out_file, resfile):
    copy_pose = Pose()
    copy_pose.assign(pose)
    f = open(out_file,'w')
    score_fxn = create_score_function('talaris2014')
    task_design = TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    parse_resfile(pose, task_design, resfile)
    pose.update_residue_neighbors()
    png = create_packer_graph(pose, score_fxn, task_design)  #Uncomment for latest Pyrosetta versions
    rotsets = RotamerSets()
    ig = pack_rotamers_setup(pose, score_fxn, task_design, rotsets)
    ig = InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(task_design, rotsets, pose, score_fxn, png)  #Uncomment for latest Pyrosetta versions
 #   setup_IG_res_res_weights(pose, task_design, rotsets, ig)
    mat = numpy.loadtxt(assign,dtype=int)
    for res1 in range(1,ig.get_num_nodes()+1):
        for res2 in range(res1+1,ig.get_num_nodes()+1):
            if (ig.get_edge_exists(res1, res2)):
                if (res2-res1>1):
                    nres1=rotsets.rotamer_set_for_moltenresidue(res1).rotamer(mat[res1-1]+1).name1()
                    nres2=rotsets.rotamer_set_for_moltenresidue(res2).rotamer(mat[res2-1]+1).name1()
                    ener=str(ig.get_two_body_energy_for_edge(res1,res2,mat[res1-1]+1,mat[res2-1]+1))
                    ca1=pose.residue(res1).xyz("CA")
                    ca2=pose.residue(res2).xyz("CA")
                    dist=math.sqrt(math.pow(ca1[0]-ca2[0],2)+math.pow(ca1[1]-ca2[1],2)+math.pow(ca1[2]-ca2[2],2))
                    f.write(nres1+" "+nres2+" "+str(res1)+" "+str(res2)+" "+str(dist)+" "+ener+'\n')
    f.close()

def get_enum_threshold(tb2log, enum):
    for line in open(tb2log):
        if "Optimum:" in line:
            s=re.split(' ',line)
            return int(int(s[1])+enum*1.e8)

parser=optparse.OptionParser()
parser.add_option('--pdb', dest = 'pdb_file',
    default = 'examples/1aho.pdb',
    help = 'The PDB file to process [default: %default]' )
parser.add_option('--pdb_out', dest = 'pdb_out',
    default = 'examples/opt.pdb',
    help = 'The PDB file to output [default: %default]' )
parser.add_option('--mat', dest = 'mat_out',
    default = 'out.mat',
    help = 'The scoring matrix to output [default: %default]' )
parser.add_option('--wcsp', dest = 'wcsp_out',
    default = 'out.wcsp',
    help = 'The wcsp file to output [default: %default]' )

parser.add_option('--tb2log', dest = 'tb2log',
    default = 'tb2.log',
    help = 'Save toulbar2 output in a log file' )

parser.add_option('--tb2sol', dest = 'tb2sol',
    default = 'sol',
    help = 'Save toulbar2 optimum in a file [default: %default]' )

parser.add_option('--resfile', dest = 'resfile',
    default = 'examples/fulld.resfile', 
    help = 'File in resfile format (see Rosetta doc) [default: %default]' )

parser.add_option('--ub', dest = 'ub',
    default = '',   
    help = 'Filename for tb2 upper bound (extension has to be .sol !)' )

parser.add_option('--no_relax',action="store_false", dest = 'relax',
    default = True,    
                  help = 'No relaxation before energy minimization [default: False]' )

parser.add_option('--no_matrix',action="store_false", dest = 'matrix',
    default = True,    
                  help = 'Do not generate a scoring matrix [default: False]' )

parser.add_option('--no_wcsp',action="store_false", dest = 'wcsp',
    default = True,    
                  help = 'Do not generate a wcsp file [default: False]' )

parser.add_option('--no_gmec',action="store_false", dest = 'gmec',
    default = True,    
                  help = 'Do not compute the GMEC [default: False]' )

parser.add_option('--no_repack',action="store_false", dest = 'load',
    default = True,    
                  help = 'Do not produce a PDB with the optimal solution [default: False]' )

parser.add_option('--enum', dest = 'enum',
    default = 0,    
    help = 'Enumerate all sequences within X units to the GMEC [default: %default]' )

parser.add_option('--pairwise',action="store_true", dest = 'pw_ener',
    default = False,    
    help = 'Dump pairwise energies and distances in Angstroms' )

parser.add_option('--scpbranch',action="store_true", dest = 'scpbranch',
    default = False,    
    help = 'Use SCP branching during search' )


parser.add_option('--nbrelax', dest = 'nbrelax',
    default = 1,    
    help = 'Number of relaxation trials [default: %default]' )

parser.add_option('--clean', action="store_true", dest = 'clean_pdb',
    default = False,
    help = 'Produces a Rosetta friendly PDB. If True, will not run anything else. [default: %default]' )


(options,args) = parser.parse_args()
init("-ex1 false -ex1aro false -ex2 false -ex2aro false")
pdb_file=options.pdb_file
pdb_out=options.pdb_out
mat_out=options.mat_out
wcsp_out=options.wcsp_out
ub=options.ub
resfile=options.resfile
pose = Pose()
tb2log=options.tb2log
tb2sol=options.tb2sol
relax=options.relax
matrix=options.matrix
wcsp=options.wcsp
gmec=options.gmec
load=options.load
enum=float(options.enum)
pw_ener=options.pw_ener
scpbranch=options.scpbranch
nbrelax=int(options.nbrelax)
clean_pdb=options.clean_pdb
pose=pose_from_pdb(pdb_file)

tb2cmd="toulbar2 "+wcsp_out

if clean_pdb:
    cleanATOM( pdb_file )
else:

    if relax:
        pose=relax_backbone(pose,nbrelax)

    if matrix:
        compute_interactions(pose,resfile, mat_out)

    if (ub):
        provide_ub(pose, pdb_file, resfile,ub)
        tb2cmd=tb2cmd+" "+ub

    if wcsp:
        wcspcmd=os.path.dirname(os.path.abspath(__file__))+"/mat2wcsp "+mat_out+" "+wcsp_out

        os.system(wcspcmd)

    
    if gmec:
        if scpbranch:
            tb2cmd=tb2cmd+" -dee: -O=-3 -B=1 -A -s -w="+tb2sol+" --cpd --scpbranch > "+tb2log
        else:
            tb2cmd=tb2cmd+" -dee: -O=-3 -B=1 -A -s -w="+tb2sol+" --cpd > "+tb2log
        os.system(tb2cmd)

    if load:
        load_assign(pose, tb2sol, pdb_out, resfile)

    if enum:
        threshold=get_enum_threshold(tb2log,enum)
        if scpbranch:
            enumcmd="toulbar2 "+wcsp_out+" -a -ub="+str(threshold)+" -dee: -A -s --cpd --scpbranch > "+tb2log+".enum"
        else:
            enumcmd="toulbar2 "+wcsp_out+" -a -ub="+str(threshold)+" -dee: -A -s --cpd > "+tb2log+".enum"
        os.system(enumcmd)

    if pw_ener:
        dump_pairwise_energies(pose, tb2sol, pdb_file[:-4]+".pw", resfile)
