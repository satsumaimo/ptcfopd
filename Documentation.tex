\documentclass[a4paper]{article}

\usepackage{hyperref}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm,a4paper]{geometry}
\parindent 0pt
\parskip 7pt plus 5pt minus 3pt
\date{June 2015}

\begin{document}
\title{How to use tb2cdp.py}
\maketitle

The provided python scripts \texttt{tb2cpd.py} and
\texttt{tb2cpd-new.py} compute the optimal sequence for a given
backbone according to the Rosetta Talaris2014 scoring function and
using the Dunbrack10 rotamer library. This documentation describes the
workflow and options of this script using a standard Linux
distribution (in our case we used Ubuntu but this should work on all
distributions). A machine with a sufficient memory (at least 8G bytes
or RAM) is required (mostly for PyRosetta usage). Please contact the
\texttt{toulbar2} team in case of issues.

\section{Quick start}

The following gives a sequence of commands that will allow you to run
one of the scripts, provided that the following packages and software
are installed on your system: a recent \texttt{g++} compiler
(supporting c++11), \texttt{git}, \texttt{make}, \texttt{cmake},
texttt{boost} graph (\texttt{libboost-graph-dev} package in most Linux
distributions), \texttt{gmp} (\texttt{libgmp3-dev}). If \texttt{git},
\texttt{gmp}, \texttt{boost} is not installed, please contact your
system administrator to have them installed.

We will now install , \texttt{PyRosetta} and \texttt{Toulbar2} which
are the two basic components of the workflow.

\subsection{PyRosetta}

To download \texttt{PyRosetta}, you should get an academic licence at the
following URL: \url{http://www.pyrosetta.org/dow}.

Be sure to download the Monolith version of \texttt{PyRosetta}.  The
version we used in the JCTC paper was version 58. The last version we
tested was 98. Between version 58 and 98, different PyRosetta
functions changed signature or simply disappeared and have been
replaced by new ones.

The \texttt{tb2cpd.py} script is the original script we used in our
JCTC paper and should be sued with PyRosetta 58. The
\texttt{tb2cpd-new.py} script has been updated to try to follow
PyRosetta changes. It should work with version 98.

Uncompress the PyRosetta archive:
\begin{verbatim}
tar xvfj PyRosetta.monolith.ubuntu.release-*.tar.bz2
cd PyRosetta.monolith.ubuntu.release-*
\end{verbatim}

In version 58 of PyRosetta, we had to slightly modify the
\texttt{SetPyRosettaEnvironment.sh} file in order to enable system
wide usage.  This seems useless in version 98. If you have version 58,
inside the \texttt{PyRosetta.monolith.ubuntu.release-58} directory,
you will have to edit \texttt{SetPyRosettaEnvironment.sh} to remove
"\texttt{rosetta}" from lines 51 and 52, shown below:

\begin{verbatim}
export DYLD_LIBRARY_PATH=$PYROSETTA:$PYROSETTA/rosetta${DYLD_LIBRARY_PATH+:$DYLD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$PYROSETTA/rosetta${LD_LIBRARY_PATH+:$LD_LIBRARY_PATH}
\end{verbatim}

\noindent Now, run the following commands to use a bash shell and make
PyRosetta visible to python:
\begin{verbatim}
exec bash
source SetPyRosettaEnvironment.sh
\end{verbatim}

If you leave the bash shell, you'll have to rexecute this on a new
shell. To install \texttt{toulbar2} in the current PyRosetta
directory, run the following commands. This will download
\texttt{toulbar2} sources, compile and make the \texttt{toulbar2}
executable accessible during this shell session (\texttt{PATH}
variable change).

\begin{verbatim}
git clone http://mulcyber.toulouse.inra.fr/anonscm/git/toulbar2/toulbar2.git
cd toulbar2
git checkout cpd
cd toulbar2
mkdir build
cd build
cmake ..
make
PATH=/`pwd`/bin/Linux/:$PATH
cd ../../..
\end{verbatim}

Finally, run the following commands to get access to the
\texttt{tb2cpd.py} and \texttt{tb2cpd-new.py} python scripts that
will allow you to go from a PDB file to a fully-redesigned guaranteed
GMEC or an exhaustive list of suboptimal solutions.

\begin{verbatim}
git clone https://bitbucket.org/satsumaimo/ptcfopd.git
cd ptcfopd/
make
\end{verbatim}

It is now time to try the suitable python script. Remember that if you
downloaded a PyRosetta version strictly greater than 58, it is likely
that you will need to use \texttt{tb2cpd-new.py} instead of
\texttt{tb2cpd.py}. When called with no arguments, the script will use
the default \texttt{examples/1aho.pdb} file. This may takes some time
to process (more than 10 minutes, among which less than two minutes
for finding and proving the GMEC on any recent amd64 core). You can
try either:

\begin{verbatim}
python tb2cpd.py
\end{verbatim}
or
\begin{verbatim}
python tb2cpd-new.py
\end{verbatim}

The resulting GMEC sequence can be obtained in the \texttt{tb2.log}
file. The corresponding PDB file will be accessible in the
\texttt{examples} directory, in the \texttt{opt.pdb} file. For other
setups, please have a look to the optional flags for the script below.


\section{Workflow}

The workflow consists in seven sequential tasks, the first three being optional.

\paragraph{PDB cleaning (optional)}
The PDB backbone in input has to be Rosetta friendly: it should 
contain only ATOM fields containing one of the 20 natural amino acids. 

\paragraph{Backbone relaxation (optional)} 
It is recommended to perform a backbone relaxation before the computational 
design begins in order to remove crystal artifacts and increase the 
compatibility between the rotamers and the scoring function.

\paragraph{Upper bound generation (optional)}
Giving a reasonably good upper bound to the cpdSolver can 
speed up the computation.  The upper bound is generated through
PyRosetta's rotamer packing routine.

\paragraph{Computation of the scoring matrix}
The scoring matrix is computed through \texttt{PyRosetta} and the
Rosetta interaction graph.

\paragraph{Conversion to WCSP format}
The scoring matrix is translated in a Cost Function Network (aka Weighted Constraint Satisfaction Problem, WCSP).

\paragraph{Resolution of the WCSP}
\texttt{Toulbar2} is used to solve the Network.

\paragraph{Production of an optimally packed output PDB}
The solution returned by \texttt{Toulbar2} is re-injected in a PDB file.


\section{Options}

\begin{tabular}{rl}
\texttt{--pdb=PDB\_FILE} &     The PDB file to process [default: examples/1aho.pdb]\\
\texttt{--pdb\_out=PDB\_OUT} & The PDB file to output [default: examples/opt.pdb]\\
\texttt{--mat=MAT\_OUT} &      The score matrix to output [default: out.mat]\\
\texttt{--wcsp=WCSP\_OUT} &    The wcsp file to output [default: out.wcsp]\\
\texttt{--tb2log=TB2LOG} &     Save toulbar2 output in a log file\\
\texttt{--tb2sol=TB2SOL} &     Save toulbar2 optimum in a file [default: sol]\\
\texttt{--resfile=RESFILE} &   File in resfile format (see Rosetta doc) [default:examples/fulld.resfile]\\
\texttt{--ub=UB} &             Filename for tb2 upper bound (extension has to be .sol !)\\
\texttt{--no\_relax} &         No relaxation before energy minimization [default: False]\\
\texttt{--no\_matrix} &        Do not generate a scoring matrix [default: False]\\
\texttt{--no\_wcsp} &          Do not generate a wcsp file [default: False]\\
\texttt{--no\_gmec} &          Do not compute the GMEC [default: False]\\
\texttt{--no\_repack} &        Do not produce a PDB with the optimal solution [default: False]\\
\texttt{--enum} &              Enumerate all sequences within 0.2 units to the GMEC [default: False]\\
\texttt{--nbrelax=NBRELAX} &   Number of relaxation trials [default: 1]\\
\texttt{--clean} &             Produces a Rosetta friendly PDB. If enabled, will not run anything else. [default: False]
\end{tabular}


\end{document}

