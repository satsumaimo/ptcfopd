# README #

Just clone the project and read the '[Documentation.pdf](https://bitbucket.org/satsumaimo/ptcfopd/raw/3c9b0ac1c3bbf17618f88ac881e8e7eb252472d7/Documentation.pdf)' file.

### What is this repository for? ###

* Allows to find a guaranteed GMEC sequence or a gapless list of all sequences in the neighbourhood of the GMEC using [toulbar2](www.inra.fr/mia/T/toulbar2/), Talaris14 force field and Dunbrack's rotamers library from a Rosetta friendly PDB file.
